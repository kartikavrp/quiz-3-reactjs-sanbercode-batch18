import React, { useContext } from "react";
import { Switch, Route } from "react-router-dom";
import Navbar from "./Navbar";
import Home from "../Content/Home";
import About from "../Content/About";
import MovieListEditor from "../Content/MovieListEditor";
//import NotFound from "../Content/NotFound";
import { LoginContext } from "../Context/LoginContext";

const Routes = () => {
  const { login } = useContext(LoginContext);

  return (
    <>
      <Navbar />
      <section>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/about">
            <About />
          </Route>
          {login === "token-valid" && (
            <Route path="/movie-list-editor">
              <MovieListEditor />
            </Route>
          )}
          {/*<Route component={NotFound} />*/}
        </Switch>
      </section>
      <footer>
        <h5>copyright &copy; 2020 by Sanbercode</h5>
      </footer>
    </>
  );
};

export default Routes;
