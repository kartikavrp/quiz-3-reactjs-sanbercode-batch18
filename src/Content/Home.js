import React from "react";
import axios from "axios";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataMovie: [],
    };
  }

  componentDidMount() {
    const url = "http://backendexample.sanbercloud.com/api/movies";
    axios
      .get(url)
      .then((res) => {
        this.setState({
          dataMovie: res.data.sort((a, b) => (a.rating > b.rating ? -1 : 1)),
        });
      })
      .catch((err) => {
        console.log(err.response);
      });
  } 

  render() {
    return (
      <div className="container">
        <h1 className="title"> Daftar Film Terbaik</h1>
        <div id="movie-list">
          {this.state.dataMovie.map((movie) => {
            return (
              <div key={movie.id}>
                <h3>{movie.title}</h3>
                <br />
                <div className="movie-img" >
                  <img src={movie.image_url} />
                </div>
                <p><b> Rating : {movie.rating}</b></p>
                <p><b> Durasi : {movie.duration} menit</b></p>
                <p><b> Genre  : {movie.genre}</b></p>
                <br />
                <p>
                  <b>Deskripsi: </b>
                  {movie.description}
                </p>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default Home;
