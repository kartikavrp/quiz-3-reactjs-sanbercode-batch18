import React from "react";

const About = () => {
  return (
    <div className="container">
      <div className="about">
        <h1 className="title">Data Peserta Sanbercode Bootcamp Reactjs</h1>
        <ol>
          <li>
            <strong>Nama :</strong> Kartika Vidya Rian Putri
          </li>
          <li>
            <strong>Email :</strong> kartikavidyarp@gmail.com
          </li>
          <li>
            <strong>Sistem Operasi yang digunakan :</strong> Windows 10
          </li>
          <li>
            <strong>Akun Gitlab :</strong> https://gitlab.com/kartikavrp
          </li>
          <li>
            <strong>Akun Telegram :</strong> @kaveerpe
          </li>
        </ol>
      </div>
    </div>
  );
};

export default About;