import React, { useState, useEffect } from "react";
import axios from "axios";

const MovieListEditor = () => {
  const [refreshData, setRefreshData] = useState(false);
  const [movie, setMovie] = useState(null);
  const [movieId, setMovieId] = useState("");
  const [input, setInput] = useState({
    title: "",
    description: "",
    year: "",
    duration: "",
    genre: "",
    rating: "",
    image_url: "",
  });
  const [editMode, setEditMode] = useState(false);
  const [error, setError] = useState(false);

  useEffect(() => {
    if (!refreshData) {
      const url = "http://backendexample.sanbercloud.com/api/movies";
      axios
        .get(url)
        .then((res) => {
          setMovie(res.data);
          setRefreshData(true);
        })
        .catch((err) => {
          console.log(err.response);
        });
    }
  }, [refreshData]);

  const handleChange = (event) => {
    let newInput = { ...input };
    let name = event.target.name;
    let value = event.target.value;

    switch (name) {
      case "year":
        value = parseInt(value);
        if (isNaN(value)) {
          value = "";
        }
        break;

      case "duration":
        value = parseInt(value);
        if (isNaN(value)) {
          value = "";
        }
        break;

      case "rating":
        value = parseInt(value);
        if (isNaN(value)) {
          value = "";
        }

        if (value < 1 || value > 10) {
          value = "";
        }
        break;
      default:
        break;
    }

    newInput[name] = value;
    setInput(newInput);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const url = "http://backendexample.sanbercloud.com/api/movies";
    const { title, description, year, duration, genre, rating, image_url } = input;

    if (title && description && year && duration && genre && rating && image_url) {
      axios
        .post(url, input)
        .then((res) => {
          setRefreshData(false);
          setInput({
            title: "",
            description: "",
            year: "",
            duration: "",
            genre: "",
            rating: "",
            image_url: "",
          });
          setError(false);
        })
        .catch((err) => {
          console.log(err.response);
        });
    } else {
      setError(true);
    }
  };

  const handleEditData = (id) => {
    const data = movie.filter((movie) => {
      return movie.id === id;
    });

    setInput({
      title: data[0].title,
      description: data[0].description,
      year: data[0].year,
      duration: data[0].duration,
      genre: data[0].genre,
      rating: data[0].rating,
      image_url: data[0].image_url,
    });
    setMovieId(data[0].id);
    setEditMode(true);
    setError(false);
  };

  const handleEdit = (event) => {
    event.preventDefault();
    const url = `http://backendexample.sanbercloud.com/api/movies/${movieId}`;
    const { title, description, year, duration, genre, rating, image_url } = input;

    if (title && description && year && duration && genre && rating && image_url) {
      axios
        .put(url, input)
        .then((res) => {
          setRefreshData(false);
          setInput({
            title: "",
            description: "",
            year: "",
            duration: "",
            genre: "",
            rating: "",
            image_url: "",
          });
          setMovieId("");
          setEditMode(false);
          setError(false);
        })
        .catch((err) => {
          console.log(err.response);
        });
    } else {
      setError(true);
    }
  };

  const handleDelete = (id) => {
    const url = `http://backendexample.sanbercloud.com/api/movies/${id}`;
    axios
      .delete(url)
      .then((res) => {
        setRefreshData(false);
        setInput({
          title: "",
          description: "",
          year: "",
          duration: "",
          genre: "",
          rating: "",
          image_url: "",
        });
        setMovieId("");
        setEditMode(false);
        setError(false);
      })
      .catch((err) => {
        console.log(err.response);
      });
  };

  const handleCancel = () => {
    setInput({
      title: "",
      description: "",
      year: "",
      duration: "",
      genre: "",
      rating: "",
      image_url: "",
    });
    setMovieId("");
    setEditMode(false);
    setError(false);
  };

  let no = 1;

  return (
    <div style={{ width: "100%", textAlign: "center" }}>
      <div className="list-movie">
        <h1>Daftar Film</h1>
        <table className="table-movie">
          <thead>
            <tr>
              <th>No.</th>
              <th>Title</th>
              <th>Description</th>
              <th>Year</th>
              <th>Duration</th>
              <th>Genre</th>
              <th>Rating</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {movie &&
              movie.map((data) => (
                <tr style={{ textAlign: "left" }} key={data.id}>
                  <td>{no++}</td>
                  <td>{data.title}</td>
                  <td>{data.description}</td>
                  <td>{data.year}</td>
                  <td>{data.duration} menit</td>
                  <td>{data.genre}</td>
                  <td>{data.rating}</td>
                  <td style={{ textAlign: "center", width: "100px" }}>
                    <button className="btn-edit" onClick={() => handleEditData(data.id)}>
                      Edit
                    </button>
                    <button className="btn-delete" onClick={() => handleDelete(data.id)}>
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
      
      <br />
      <div className="form-container">
        <h1>Movies Form</h1>
        <form onSubmit={editMode ? handleEdit : handleSubmit} className="form-input">
          <div className="input-container">
	          <div className="label"> 
              <label for="title"> Title </label> :{" "}
            </div>
            <div className="input">
              <input type="text" name="title" value={input.title}  onChange={handleChange} placeholder="Title" />
            </div>
          </div>

          <div className="input-container">
	          <div className="label"> 
              <label for="description"> Description </label> :{" "}
              </div>
            <div className="input">
              <textarea className="enam" type="text" name="description" value={input.description} onChange={handleChange} placeholder="Description" />
            </div>
          </div>

          <div className="input-container">
	          <div className="label"> 
              <label for="year"> Year </label> :{" "}
              </div>
            <div className="input">
              <input type="text" name="year" value={input.year} onChange={handleChange} placeholder="Year" />
            </div>
          </div>

          <div className="input-container">
	          <div className="label"> 
              <label for="duration"> Duration (minutes) </label> :{" "}
              </div>
            <div className="input">
              <input  type="text" name="duration" value={input.duration} onChange={handleChange} placeholder="Duration" />
            </div>
          </div>

          <div className="input-container">
	          <div className="label"> 
              <label for="genre"> Genre </label> :{" "}
              </div>
            <div className="input">
              <input type="text" name="genre" value={input.genre} onChange={handleChange} placeholder="Genre"/>
            </div>
          </div>

          <div className="input-container">
	          <div className="label"> 
              <label for="rating"> Rating (1-10) </label> :{" "} 
              </div>
            <div className="input">
              <input type="text" name="rating" value={input.rating} onChange={handleChange} placeholder="Rating"/>
            </div>
          </div>

          <div className="input-container">
	          <div className="label"> 
              <label for="imgurl"> Image URL</label> :{" "} 
            </div>
            <div className="input">
              <textarea className="enam" type="text" name="imgurl" value={input.image_url} onChange={handleChange} placeholder="Image URL" />
            </div>
          </div>
        
          {error && (
            <div className="error">
              Field tidak boleh kosong!
            </div>
          )}

          {editMode ? (
            <div className="">
              <button className="btn-form" onClick={handleCancel}> Cancel </button>
              <button className="btn-form" type="submit"> Edit </button>
            </div>
          ) : (
            <button className="btn-form" type="submit"> Tambah Data Baru </button>
          )}                           
        </form>
      </div>      
    </div>
  );
};

export default MovieListEditor;
