import React from 'react';
import logo from './logo.svg';
import './App.css';
import { LoginProvider } from "./Context/LoginContext";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./Routes/Route";
import "./assets/css/style.css";

function App() {
  return (
    <Router>
      <LoginProvider>
        <Routes />
      </LoginProvider>
    </Router>
  );
}

export default App;